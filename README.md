# Secure macOS Shell Theme

A terminal theme for macOS that allows you to have a (somewhat) more secure shell.


It will lower the scrollback length of that window so that running clear will completely clear it,
as well as blacking out whatever's highlighted, and it also doesn't display a small preview when minimized

