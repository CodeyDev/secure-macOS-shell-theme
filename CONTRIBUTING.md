1. Only Commit To Your Own Branch, Unless Given Permission
2. Use Common Sense- Just because a good, logical rule isn't on here, does *NOT* mean you can break it
3. Think before you post (or commit)
4. Only upload stuff you have the proper rights for
5. No Using Loopholes